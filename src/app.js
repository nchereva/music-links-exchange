import express from 'express';
import api from './route/api';
import morgan from 'morgan';
let app = express();

app.use(morgan('combined'));

api.register(app);

app.use(express.static(__dirname + '/web'));

app.listen(3000, ()=> {
    console.log('Application started');
});

export default app;