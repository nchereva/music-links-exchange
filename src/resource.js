export default class Resource {
    getName() {
        return 'Abstract resource';
    }

    search() {
        throw new Error('Search not implemented.');
    }

    lookup() {
        throw new Error('Lookup not implemented.');
    }

    checkUrl() {
        throw new Error('CheckUrl not implemented.');
    }

    preprocessName(name) {
        return Promise.resolve(name.replace(/\s+[[(].+[)\]]/, ''));
    }

    responseWrapper(response) {
        console.log(this.getName(), response);
        if (response) {
            return Promise.resolve({type: this.getName().toLowerCase(), url: response});
        }
    }
}