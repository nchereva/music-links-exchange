import youTube from '../youtube.js';
import spotify from '../spotify.js';
import vk from '../vk.js';
import _ from 'lodash';
import express from 'express';
let router = express.Router();

let assigner = _.assign({}),
    providers = [youTube, spotify, vk];

async function search(input) {
    let results = await Promise.all(_.invokeMap(providers, 'search', input));
    return _.compact(results);
}

async function urlConverter(input) {
    try {
        let targetResource = providers.find(res=>res.checkUrl(input)),
            restResources = _.without(providers, targetResource),
            name = await targetResource.lookup(input),
            results = await Promise.all(_.invokeMap(restResources, 'search', name));
        return results;
    }
    catch (e) {
        console.log(e);
    }
}

router.get('/', async ({query : {echo, q, url}}, res)=> {
    try {
        let response;

        if (url) {
            response = await urlConverter(url);
        }

        if (q) {
            response = await search(q);
        }

        if (echo) {
            response = echo;
        }

        res.json(response);
    } catch (e) {
        console.log(e);
    }
});

export default {
    register(app) {
        app.use('/api', router);
        return app;
    }
};