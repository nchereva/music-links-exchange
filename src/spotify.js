import Resource from './resource';
import spotify from 'spotify';
import {result} from 'lodash/fp';

// https://open.spotify.com/track/0jMGxleAwmObKcd4kTp7pl
// spotify:track:3UbdhEd5eo29MSyJgnxbab
let urlRegex = /(spotify).+[:=\/]/;

class Spotify extends Resource {
    getName() {
        return 'Spotify';
    }

    search(query) {
        var promise = new Promise((resolve, reject) => {
            spotify.search({
                type: 'track',
                query
            }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            })
        });

        promise = promise.then(result('tracks.items[0].external_urls.spotify'));

        promise = promise.then(this.responseWrapper.bind(this));

        return promise;
    }

    lookup(link) {
        let [id, ] = /([^:\/]+)$/.exec(link),
            promise;

        if (id) {
            promise = new Promise((res, rej) => {
                spotify.lookup({
                    type: 'track',
                    id
                }, (err, data) => {
                    if (err) {
                        rej(err);
                    } else {
                        res(data);
                    }
                })
            });

            promise = promise.then((data) => {
                var name = result('name', data),
                    artist = result('artists[0].name', data);
                return `${artist} ${name}`;
            });

            return promise;
        }
    }

    checkUrl(url) {
        return url.match(urlRegex);
    }
}

export default new Spotify();