import request from 'request';
import _ from 'lodash';
import md5 from 'md5';
import Resource from './resource';

const userSecret = process.env.VK_SECRET,
    token = process.env.VK_TOKEN,
    METHODS = {
        SEARCH: 'audio.search',
        LOOKUP: 'audio.getById'
    },
    urlRegex = /vk.+(audio)[-]?\d+_\d+/;


class VK extends Resource {
    getName() {
        return 'VK';
    }

    getUrl(method, data = {}) {
        let dataString = _.map(data, (val, key)=>`${key}=${val}`).join('&'),
            sigString = `/method/${method}?${dataString}&access_token=${token}`,
            sig = md5(`${sigString}${userSecret}`);

        return `https://api.vk.com/${sigString}&sig=${sig}`

    }

    search(query) {
        var promise = new Promise((resolve, reject) => {
            request({
                url: this.getUrl(METHODS.SEARCH, {q: query}),
                json: true
            }, (err, res, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            })
        });

        promise = promise.then((data)=> {
            return _.result(data, 'response[1]')
        });
        promise = promise.then(this.responseHandler);
        promise = promise.then(this.responseWrapper.bind(this));

        return promise;
    }

    responseHandler(response) {
        if (!response) return Promise.resolve(null);
        let {owner_id, aid} = response;
        return Promise.resolve(`https://vk.com/audio${owner_id}_${aid}`);
    }

    lookup(link) {
        let [id, ] = /(\d+_\d+)$/.exec(link),
            promise;

        if (id) {
            promise = new Promise((res, rej) => {
                request({
                    url: this.getUrl(METHODS.LOOKUP, {
                        audios: id
                    }),
                    json: true
                }, (err, response, data) => {
                    if (err) {
                        rej(err);
                    } else {
                        res(data);
                    }
                })
            });

            promise = promise.then((data) => {
                let audio = _.result(data, 'response[0]'),
                    {artist, title} = audio;
                return Promise.resolve(`${artist} ${title}`);
            });

            return promise;
        }
    }

    checkUrl(url) {
        return url.match(urlRegex);
    }
}

export default new VK();