(()=> {
    'use strict';
    let input = document.querySelector('input'),
        results = document.querySelector('.results');

    function getRepresentation(source) {
        return `<a href="${source.url}" class="${source.type} source" target="_blank"></a>`
    }

    input.addEventListener('input', (ev)=> {
        let value = input.value,
            param = value.match(/(youtu|spotify|vk)/) ? 'url' : 'q';
        if (value.length < 3) {
            return Promise.resolve();
        } else {
            return fetch(`/api/?${param}=${value}`)
                .then((res)=>res.json())
                .then(value=>results.innerHTML = value.map(getRepresentation).join(''))
                .catch(console.error.bind(console));
        }
    })
})();