import Resource from './resource';
import YouTube from 'youtube-node';
import {result} from 'lodash/fp';

let youTube = new YouTube();

// https://www.youtube.com/watch?v=oM6eZJRurFk
// https://youtu.be/oM6eZJRurFk
let urlRegex = /(youtube|youtu).+[=\/]/;

youTube.setKey(process.env.YOUTUBE_SECRET);
youTube.addParam('type', 'video');
youTube.addParam('order', 'viewCount');

class Youtube extends Resource {
    getName() {
        return 'YouTube';
    }

    search(query) {
        var promise = new Promise((resolve, reject) => {
            youTube.search(query, 1, (error, result) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(result);
                }
            });
        });

        promise = promise.then(({
            items = [], item = items[0], id = result('id.videoId', item)
            }) => {
            return `http://youtu.be/${id}`;
        });

        promise = promise.then(this.responseWrapper.bind(this));

        return promise;
    }

    lookup(link) {
        let [id, ] = /([^=:\/]+)$/.exec(link),
            promise;

        if (id) {
            promise = new Promise((res, rej) => {
                youTube.getById(id, (err, data) => {
                    if (err) {
                        rej(err);
                    } else {
                        res(data);
                    }
                })
            });

            promise = promise.then((data) => {
                return result('items[0].snippet.title', data);
            });

            promise = promise.then(this.preprocessName);

            return promise;
        } else {
            return Promise.resolve(null);
        }
    }

    checkUrl(url) {
        return url.match(urlRegex);
    }
}

export default new Youtube();