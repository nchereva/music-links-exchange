import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import youTube from '../src/youtube';
import spotify from '../src/spotify';

chai.use(chaiAsPromised);

let {expect} = chai;

describe("app", () => {
    describe("youtube", () => {
        let promise = youTube.search('Annul'),
            lookupPromise = youTube.lookup('https://www.youtube.com/watch?v=oM6eZJRurFk');
        it("should return Promise", () => {
            expect(promise).to.be.an.instanceof(Promise);
        });
        it("should match youtube url", () => {
            expect(youTube.checkUrl('https://youtu.be/oM6eZJRurFk')).to.be.ok;
        });
        it("promise should be fullfilled", () => {
            return promise;
        });
        it("should lookup track urls", () => {
            return expect(lookupPromise).to.eventually.equal('Ed Harrison - Annul');
        });
    });
    describe("spotify", () => {
        let promise = spotify.search('Annul'),
            lookupPromise = spotify.lookup('spotify:track:3UbdhEd5eo29MSyJgnxbab');
        it("should return Promise", () => {
            expect(promise).to.be.an.instanceof(Promise);
        });
        it("should match spotify url", () => {
            expect(spotify.checkUrl('spotify:track:3UbdhEd5eo29MSyJgnxbab')).to.be.ok;
        });
        it("promise should be fullfilled", () => {
            return promise;
        });
        it("should lookup track urls", () => {
            return expect(lookupPromise).to.eventually.equal('Ed Harrison Annul');
        });
    });
});